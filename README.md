# RedExplorer Manager (750503, Дмитрий Кугач)

## Overview
Файловый менеджер под операционную систему Windows.
Главное меню содержит две панели с возможностью копирования или перемещения файлов между ними.
Основные операции доступны через текстовые поля над панелями.

## File operations
* Создать новую дирректорию

* Создать новый файл

* Перименовать

* Удалить текущие файлы

* Скопировать выбранные файлы на другую панель

* Переместить выбранные файлы на другую панель

## Advanced operations
**command == keyword + argument**
* SELECT

* MOVE

* COPY

* DELETE

* OPEN

## Advanced operations
* Ctrl + Shift + N - create directory

* Ctrl + N - create file

* Shift + F6 - rename

* Delete - delete

* F5 - copy

* F6 - move