package redexplorer;

import javafx.application.Application;
import javafx.scene.control.*;
import javafx.scene.Scene;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

public class Main extends Application {
    private static final KeyCombination SHORTCUT_COPY = new KeyCodeCombination(KeyCode.F5);
    private static final KeyCombination SHORTCUT_MOVE = new KeyCodeCombination(KeyCode.F6);
    private static final KeyCombination SHORTCUT_DELETE = new KeyCodeCombination(KeyCode.DELETE);
    private static final KeyCombination SHORTCUT_NEW_FILE = new KeyCodeCombination(KeyCode.N,
            KeyCombination.SHORTCUT_DOWN);
    private static final KeyCombination SHORTCUT_NEW_DIRECTORY = new KeyCodeCombination(KeyCode.N,
            KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN);
    private static final KeyCombination SHORTCUT_RENAME = new KeyCodeCombination(KeyCode.F6, KeyCombination.SHIFT_DOWN);

    private FileView mFileView;

    @Override
    public void start(Stage primaryStage) {
        VBox root = new VBox();

        mFileView = new FileView();

        VBox.setVgrow(mFileView, Priority.ALWAYS);

        root.getChildren().addAll(getMenuBar(), mFileView, getToolBar());
        Scene scene = new Scene(root, 800, 600);
        scene.addEventFilter(KeyEvent.KEY_RELEASED, e -> {
            if (SHORTCUT_DELETE.match(e)) {
                mFileView.delete();
            } else if (SHORTCUT_NEW_FILE.match(e)) {
                mFileView.createFile();
            } else if (SHORTCUT_NEW_DIRECTORY.match(e)) {
                mFileView.createDirectory();
            } else if (SHORTCUT_RENAME.match(e)) {
                mFileView.rename();
            } else if (SHORTCUT_COPY.match(e)) {
                mFileView.copy();
            } else if (SHORTCUT_MOVE.match(e)) {
                mFileView.move();
            }
        });

        primaryStage.setTitle("RedExplorer");
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("redexplorer.png")));
        primaryStage.show();
    }

    private MenuBar getMenuBar(){
        Menu fileMenu = new Menu("File");
        MenuItem newFile = new MenuItem("New File");
        newFile.setOnAction(e -> mFileView.createFile());
        newFile.setAccelerator(SHORTCUT_NEW_FILE);

        MenuItem newFolder = new MenuItem("New Folder     ");
        newFolder.setOnAction(e -> mFileView.createDirectory());
        newFolder.setAccelerator(SHORTCUT_NEW_DIRECTORY);

        MenuItem renameItem = new MenuItem("Rename");
        renameItem.setOnAction(e -> mFileView.rename());
        renameItem.setAccelerator(SHORTCUT_RENAME);

        MenuItem deleteItem = new MenuItem("Delete");
        deleteItem.setOnAction(e -> mFileView.delete());
        deleteItem.setAccelerator(SHORTCUT_DELETE);

        MenuItem copyItem = new MenuItem("Copy");
        copyItem.setOnAction(e -> mFileView.copy());
        copyItem.setAccelerator(SHORTCUT_COPY);

        MenuItem moveItem = new MenuItem("Move");
        moveItem.setOnAction(e -> mFileView.move());
        moveItem.setAccelerator(SHORTCUT_MOVE);

        fileMenu.getItems().addAll(newFile, newFolder, renameItem, deleteItem, copyItem, moveItem);

        Menu helpMenu = new Menu("Help");
        MenuItem aboutMenuItem = new MenuItem("About");
        aboutMenuItem.setOnAction(e -> {
            DialogHelper.showAlert(Alert.AlertType.INFORMATION, "About", null,
                    "RedExplorer\n\n" + "Copyright © 2019 by Aaron\n");
        });
        helpMenu.getItems().addAll(aboutMenuItem);

        return new MenuBar(fileMenu, helpMenu);
    }

    private ToolBar getToolBar() {
        Label labelCreateFile = new Label("Ctrl + N Create File");
        Label labelCreateDirectory = new Label("Ctrl + Shift + N Create Directory");
        Label labelRename = new Label("Shift + F6 Rename");
        Label labelDelete = new Label("Delete Delete");
        Label labelCopy = new Label("F5 Copy");
        Label labelMove = new Label("F6 Move");
        return new ToolBar(labelCreateFile, new Separator(), labelCreateDirectory, new Separator(),
                labelRename, new Separator(), labelDelete, new Separator(), labelCopy, new Separator(),
                labelMove, new Separator());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
